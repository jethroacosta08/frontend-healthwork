import Vue from 'vue'
import Vuex from 'vuex'
import patient from './modules/patient.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    patient
  }
})
