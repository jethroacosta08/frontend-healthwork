import axios from 'axios'
import router from '../../router/index'

export default {
    state: {
        patients: [],
        forReviewCount : 0,
        caseClosedCount : 0,
    },
    mutations: {
        setPatients(state, payload) {
            state.patients = payload
        },
        setForReviewCount(state, payload) {
            state.forReviewCount = payload
        },
        setCaseClosedCount(state, payload) {
            state.caseClosedCount = payload
        }
    },
    actions: {
        loadPatients(context, params) {
            var url = "https://backend-serverless-vkupy7dkca-ue.a.run.app/api/patient?";
            if(params)
            {
                for(let key in params)
                {
                    if(params.hasOwnProperty(key))
                    {
                        url += `${key}=${params[key]}`
                    }
                    url+="&"
                }
            }

            axios
                .get(url)
                .then(response => {
                    var result = response.data.items;

                    result = result.map(item => {

                        // Temporary Data
                        if (!item.caseClosed) {
                            item.caseClosed = false
                        }

                        // Temporary Data
                        if (!item.casestatus) {
                            item.casestatus = 'for-review'
                        }

                        if(item.actions[0])
                        {
                            if(item.actions[0] == "pui"){
                                item.severity = 'PUI'
                            } else if(item.actions[0] == "pum"){
                                item.severity = 'PUM'
                            }
                        }

                        // Virtual case number
                        if(params.page)
                        {
                            item.caseno = (params.page * 10) + result.indexOf(item) + 1
                        }
                        else
                        {
                            item.caseno = result.indexOf(item) + 1
                        }

                        return item

                    })

                    if(params.mutate)
                    {
                        context.commit('setPatients', result)
                        if(params.caseClosed)
                        {
                            context.commit('setCaseClosedCount', response.data.count)
                        }
                        else
                        {
                            context.commit('setForReviewCount', response.data.count)
                        }
                    }

                    if(params.mutateCloseCaseCounter)
                    {
                        if(params.caseClosed)
                        {
                            context.commit('setCaseClosedCount', response.data.count)
                        }
                    }

                });
        },
        changeAssessment(context, config){
            console.log(config)

            var newActions = config.oldActions
            newActions[0] = config.newAssessment

            axios
                .put(`https://backend-serverless-vkupy7dkca-ue.a.run.app/api/patient/${config.pid}`, {
                    actions: newActions
                })
                .then(response => {
                    if(response.data.success){
                        window.alert('Updated!')
                        location.reload()
                    }
                    else {
                        window.alert('Error: see log for details')
                        console.error(response.data)
                    }
                })
        },
        confirmRecommendation(context, pid){

            // Go back to previous page while calling the api
            router.go(-1)

            axios
                .put(`https://backend-serverless-vkupy7dkca-ue.a.run.app/api/patient/${pid}`, {
                    caseClosed: true
                })
                .then(response => {
                    if(response.data.success){
                        window.alert('Case confirmed and closed!')
                        location.reload()
                    }
                    else {
                        window.alert('Error: see log for details')
                        console.error(response.data)
                    }
                })
        }
    },
}