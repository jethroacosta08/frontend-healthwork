export default {
    patients: [
      {
        "caseno": "1",
        "casestatus": "for-review",
        "caserisk": "high",
        "casesubmitted": '03-09-2020',

        "firstname": "tj",
        "lastname": "monserrat",
        "status": "married",
        "gender": "male",
        "city": "calamba",
        "province": "laguna",
        "region": "IV-A",
        "age": 33,
        "travelHistory": [
           // can be a list of city names, let's make it consistent later
        ],
        "birthdate": "03-06-1986",
        "mobilenumber": "09776533183",
        "symptoms": [
           // list of symptoms, like "more-than-38", "cannot-breath", standardize later
        ],
        "withHistoryContact": false,
        "comorbitities": [
          // comorbitities list in slugified form, standardize later
        ]
      },
      {
        "caseno": "2",
        "casestatus": "being-reviewed",
        "caserisk": "medium",
        "casesubmitted": '03-09-2020',

        "firstname": "Stephen John",
        "lastname": "Raymundo",
        "status": "single",
        "gender": "male",
        "city": "Imus",
        "province": "Cavite",
        "region": "CALABARZON",
        "age": 21,
        "travelHistory": [
           // can be a list of city names, let's make it consistent later
        ],
        "birthdate": "10-08-1998",
        "mobilenumber": "09123456789",
        "symptoms": [
           // list of symptoms, like "more-than-38", "cannot-breath", standardize later
        ],
        "withHistoryContact": false,
        "comorbitities": [
          // comorbitities list in slugified form, standardize later
        ]
      },
      {
        "caseno": "3",
        "casestatus": "for-review",
        "caserisk": "low",
        "casesubmitted": '03-08-2020',

        "firstname": "Angel",
        "lastname": "Locsin",
        "status": "single",
        "gender": "female",
        "city": "Quezon City",
        "province": "Metro Manila",
        "region": "NCR",
        "age": 35,
        "travelHistory": [
           // can be a list of city names, let's make it consistent later
        ],
        "birthdate": "07-01-1985",
        "mobilenumber": "09123456789",
        "symptoms": [
           // list of symptoms, like "more-than-38", "cannot-breath", standardize later
        ],
        "withHistoryContact": false,
        "comorbitities": [
          // comorbitities list in slugified form, standardize later
        ]
      },
      
      {
        "caseno": "4",
        "casestatus": "reviewed",
        "caserisk": "low",
        "casesubmitted": '03-08-2020',

        "firstname": "Mocha",
        "lastname": "Uson",
        "status": "single",
        "gender": "female",
        "city": "Quezon City",
        "province": "Metro Manila",
        "region": "NCR",
        "age": 35,
        "travelHistory": [
           // can be a list of city names, let's make it consistent later
        ],
        "birthdate": "07-01-1985",
        "mobilenumber": "09123456789",
        "symptoms": [
           // list of symptoms, like "more-than-38", "cannot-breath", standardize later
        ],
        "withHistoryContact": false,
        "comorbitities": [
          // comorbitities list in slugified form, standardize later
        ]
      }
    ]
    // patients: [
    //     {
    //       no: '1',
    //       last_name: 'Raymundo',
    //       first_name: 'Stephen John',
    //       middle_name: 'Bernardo',
    //       birthday: '08 Oct 1998',
    //       age: '21',
    //       gender: 'Male',
    //       marital_status: 'Single',
    //       assessment: 'High',
    //       q1: true,
    //       q2: true,
    //       q3: true,
    //       q4: true,
    //       q5: true,
    //       q6: 'International',
    //       q7: 'China',
    //       unit: 'BLK F LOT 23',
    //       street: 'PHASE 5',
    //       village: 'ACM Woodstock Homes',
    //       baranggay: 'Alapan I-A',
    //       city: 'Imus',
    //       province: 'Cavite',
    //       contact: '09771234567',
    //       email: 'stephenjohnraymundo@outlook.com',
    //       is_being_reviewed: false,
    //       is_reviewed: false
    //     },
    //     {
    //       no: '2',
    //       last_name: 'Lucero',
    //       first_name: 'Maria',
    //       middle_name: 'Cortez',
    //       birthday: '19 Sep 1960',
    //       age: '59',
    //       gender: 'Female',
    //       marital_status: 'Married',
    //       assessment: 'Medium',
    //       q1: true,
    //       q2: true,
    //       q3: true,
    //       q4: true,
    //       q5: true,
    //       q6: 'International',
    //       q7: 'China',
    //       unit: 'BLK 42 LOT 7',
    //       street: 'Maginhawa St.',
    //       village: `Teacher's Village East`,
    //       baranggay: '',
    //       city: 'Quezon City',
    //       province: 'NCR',
    //       contact: '09771234567',
    //       email: 'maria.lucero@yahoo.com',
    //       is_being_reviewed: true,
    //       is_reviewed: false
    //     },
    //     {
    //       no: '3',
    //       last_name: 'Xi',
    //       first_name: 'Jin Ping',
    //       middle_name: '',
    //       birthday: '19 Sep 1960',
    //       age: '72',
    //       gender: 'Female',
    //       marital_status: 'Married',
    //       assessment: 'Low',
    //       q1: true,
    //       q2: true,
    //       q3: true,
    //       q4: true,
    //       q5: true,
    //       q6: 'International',
    //       q7: 'China',
    //       unit: 'BLK 42 LOT 7',
    //       street: 'Maginhawa St.',
    //       village: `Teacher's Village East`,
    //       baranggay: '',
    //       city: 'Quezon City',
    //       province: 'NCR',
    //       contact: '09771234567',
    //       email: 'maria.lucero@yahoo.com',
    //       is_being_reviewed: true,
    //       is_reviewed: false
    //     },
    //     {
    //       no: '4',
    //       last_name: 'Dela Cruz',
    //       first_name: 'Juan',
    //       middle_name: 'Pedro',
    //       birthday: '19 Sep 1960',
    //       age: '59',
    //       gender: 'Male',
    //       marital_status: 'Married',
    //       assessment: 'Low',
    //       q1: true,
    //       q2: true,
    //       q3: true,
    //       q4: true,
    //       q5: true,
    //       q6: 'International',
    //       q7: 'China',
    //       unit: 'BLK 42 LOT 7',
    //       street: 'Maginhawa St.',
    //       village: `Teacher's Village East`,
    //       baranggay: '',
    //       city: 'Quezon City',
    //       province: 'NCR',
    //       contact: '09771234567',
    //       email: 'maria.lucero@yahoo.com',
    //       is_reviewed: true
    //     }
    //   ]
}