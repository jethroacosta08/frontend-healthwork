import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Patient from '../views/Patient.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/open',
    name: 'Open',
    component: Home
  },
  {
    path: '/closed',
    name: 'Closed',
    component: Home
  },
  {
    path: '/:patient',
    name: 'Patient',
    component: Home
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
